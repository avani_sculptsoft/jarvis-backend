﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jarvis.Resource;
using Jarvis.Service.Abstract;
using Jarvis.Shared;
using Jarvis.Shared.StatusEnums;
using Jarvis.ViewModels;
using Jarvis.ViewModels.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jarvis.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WorkOrderController : ControllerBase
    {
        private readonly IWorkOrderService workOrderService;
        public WorkOrderController(IWorkOrderService _workOrderService)
        {
            this.workOrderService = _workOrderService;
        }

        public async Task<IActionResult> CreateWorkOrder([FromBody]WorkOrderRequestModel requestModel)
        {
            Response_Message response = new Response_Message();
            try
            {
                Guid guidOutput;
                var responseModel = await workOrderService.CreateWorkOrder(requestModel);
                bool isValid = Guid.TryParse(responseModel.workorderguid, out guidOutput);
                if(isValid)
                {
                    response.data = guidOutput;
                    response.success = (int)ResponseStatusNumber.Success;
                }else if(responseModel.status == (int)ResponseStatusNumber.NotFound)
                {
                    response.data = ResponseMessages.nodatafound;
                    response.success = (int)ResponseStatusNumber.NotFound;
                }
                else if (responseModel.status == (int)ResponseStatusNumber.Error)
                {
                    //response.data = ResponseMessages.Error;
                    response.data = responseModel.message;
                    response.success = (int)ResponseStatusNumber.Error;

                }
            }
            catch(Exception e)
            {
                Logger.Log("Error in Create WorkOrder : " + e.Message);
                response.message = ResponseMessages.Error;
                response.success = (int)ResponseStatusNumber.Error;
            }
            return Ok(response);
        }

        [HttpGet("{userid}/{status?}/{pagesize?}/{pageindex?}")]
        public IActionResult GetAllWorkOrders(string userid,int status,int pagesize, int pageindex)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                ListViewModel<WorkOrderResponseModel> response = new ListViewModel<WorkOrderResponseModel>();
                response = workOrderService.GetAllWorkOrders(userid,status,pagesize,pageindex);
                response.pageIndex = pageindex;
                response.pageSize = pagesize;
                if (response.list!=null && response.list.Count > 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }else if(pageindex > 1 && response.list.Count == 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
            }
            return Ok(responsemodel);
        }

        [HttpGet("{userid}/{workorder_id}")]
        public async Task<IActionResult> GetWorkOrderDetailsByIdAsync(string userid,string workorder_id)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                WorkOrderResponseModel response = new WorkOrderResponseModel();
                response =await workOrderService.GetWorkOrderByID(workorder_id,userid);
                if (response!=null && response.work_order_uuid!=null && response.work_order_uuid!= Guid.Empty)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
            }
            return Ok(responsemodel);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateWorkOrderByManager([FromBody]UpdateWorkOrderRequestModel requestModel)
        {
            Response_Message response = new Response_Message();
            try
            {
                int result = await workOrderService.UpdateWorkOrderByManager(requestModel);
                if (result > 0)
                {
                    response.success = result;
                }else if(result == (int)ResponseStatusNumber.NotFound)
                {
                    response.message = ResponseMessages.nodatafound;
                    response.success = result;
                }else
                {
                    response.message = ResponseMessages.Error;
                    response.success = result;
                }
            }catch(Exception e)
            {
                throw e;
            }
            return Ok(response);
        }

        [HttpGet("{userid}/{pagesize?}/{pageindex?}")]
        public IActionResult GetTodayNewWorkOrders(string userid, int pagesize, int pageindex)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                ListViewModel<WorkOrderResponseModel> response = new ListViewModel<WorkOrderResponseModel>();
                response = workOrderService.GetTodayNewWorkOrders(userid, pagesize, pageindex);
                response.pageSize = pagesize;
                response.pageIndex = pageindex;
                if (response.list != null && response.list.Count > 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else if (pageindex > 1 && response.list.Count == 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
                //HTTPRES = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return Ok(responsemodel);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateWorkOrderByMaintenance([FromBody]UpdateWorkOrderByMaintenanceRequestModel requestModel)
        {
            Response_Message response = new Response_Message();
            try
            {
                int result = await workOrderService.UpdateWorkOrderByMaintenance(requestModel);
                if(result > 0)
                {
                    response.success = result;
                }
                else if(result == (int)ResponseStatusNumber.NotFound)
                {
                    response.success = result;
                    response.message = ResponseMessages.nodatafound;
                }else if(result == (int)ResponseStatusNumber.Exceeded)
                {
                    response.success = (int)ResponseStatusNumber.False;
                    response.message = ResponseMessages.AlreadyApprovedWorkOrder;
                }
                else
                {
                    response.success = result;
                    response.message = ResponseMessages.Error;
                }
            }
            catch(Exception e)
            {
                response.message = ResponseMessages.Error;
                response.success = (int)ResponseStatusNumber.Error;
                Logger.Log("Error in UpdateWorkOrderByMaintenance ", e.Message);
            }
            return Ok(response);
        }

        [HttpGet("{userid}/{assetid}/{pagesize?}/{pageindex?}")]
        public IActionResult GetWorkOrderByAssetId(string userid,string assetid,int pagesize,int pageindex)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                ListViewModel<WorkOrderResponseModel> response = new ListViewModel<WorkOrderResponseModel>();
                response = workOrderService.GetWorkorderByAssetId(userid, assetid, pagesize, pageindex);
                response.pageIndex = pageindex;
                response.pageSize = pagesize;
                if (response.list != null && response.list.Count > 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else if (pageindex > 1 && response.list.Count == 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
                //HTTPRES = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return Ok(responsemodel);
        }

        [HttpGet("{userid}/{assetid}/{searchstring}/{pagesize?}/{pageindex?}")]
        public IActionResult SearchWorkOrderByAssetId(string userid, string assetid,string searchstring, int pagesize, int pageindex)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                ListViewModel<WorkOrderResponseModel> response = new ListViewModel<WorkOrderResponseModel>();
                response = workOrderService.SearchWorkorderByAssetId(userid, assetid,searchstring, pagesize, pageindex);
                response.pageSize = pagesize;
                response.pageIndex = pageindex;
                if (response.list != null && response.list.Count > 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else if (pageindex > 1 && response.list.Count == 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
                //HTTPRES = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return Ok(responsemodel);
        }

        [HttpGet("{userid}/{searchstring}/{pagesize?}/{pageindex?}")]
        public IActionResult SearchWorkOrders(string userid, string searchstring, int pagesize, int pageindex)
        {
            Response_Message responsemodel = new Response_Message();
            try
            {
                ListViewModel<WorkOrderResponseModel> response = new ListViewModel<WorkOrderResponseModel>();
                response = workOrderService.SearchWorkOrders(userid,searchstring, pagesize, pageindex);
                response.pageIndex = pageindex;
                response.pageSize = pagesize;
                if (response.list != null && response.list.Count > 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else if (pageindex > 1 && response.list.Count == 0)
                {
                    responsemodel.success = (int)ResponseStatusNumber.Success;
                    responsemodel.data = response;
                }
                else
                {
                    responsemodel.message = ResponseMessages.nodatafound;
                    responsemodel.success = (int)ResponseStatusNumber.NotFound;
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error in GetAllAssets ", e.Message);
                responsemodel.success = (int)ResponseStatusNumber.Error;
                responsemodel.message = ResponseMessages.Error;
                //HTTPRES = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return Ok(responsemodel);
        }
    }
}