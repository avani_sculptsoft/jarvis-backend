using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jarvis.db.Models;
using Jarvis.Repo.Abstract;
using Jarvis.Repo.Concrete;
using Jarvis.Service.Abstract;
using Jarvis.Service.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Jarvis.Repo;
using AutoMapper;
using Jarvis.Service.MappingProfile;
using System.IO;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using Amazon.S3;
using Microsoft.Extensions.Hosting;

namespace Jarvis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ServiceLayerProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddHttpClient();

            services.AddMvc(option => option.EnableEndpointRouting = false);
            services.AddControllers(options =>
            {
                options.RespectBrowserAcceptHeader = true; // false by default
            });


            services.AddDbContext<DBContextFactory>(opt =>
            //opt.UseNpgsql("User ID = postgres;Password=icreate;Server=15.206.187.80;Port=5432;Database=jarvis;Integrated Security=true;Pooling=true;"));
            opt.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(typeof(IBaseGenericRepository<>), typeof(BaseGenericRepository<>));
            services.AddScoped(typeof(IFormAttributesRepository<>), typeof(FormAttributesRepository<>));
            services.AddScoped(typeof(IInspectionFormRepository), typeof(InspectionFormRepository));
            services.AddScoped(typeof(IWorkOrderRepository), typeof(WorkOrderRepository));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<IFormAttributesService, FormAttributesService>();
            services.AddTransient<IInspectionFormService, InspectionFormService>();
            services.AddTransient<IWorkOrderService, WorkOrderService>();
            services.AddTransient<IAssetService,AssetService>();
            services.AddTransient<IInspectionService, InspectionService>();
            services.AddTransient<IBaseService, BaseService>();
            services.AddTransient<IJarvisUOW, JarvisUOW>();
            services.AddTransient<ServiceLayerProfile>();

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            // get app setting configuration


            services.AddOptions();

            var section = Configuration.GetSection("AWS_Config");
            services.Configure<AWSConfigurations>(section);

            //services.AddDefaultAWSOptions(Configuration.GetAWSOptions());

            //services.TryAddAWSService<IAmazonS3>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //StringValues platform;
            //app.Use(async (context, next) =>
            //{
            //    context.Request.Headers.TryGetValue("platform", out platform);

            //    //context.Request.Headers.Add("platform", platform);

            //    //context.Request.Headers(() =>
            //    //{
            //    //    context.Response.Headers.Add("MyHeader", "GotItWorking!!!");
            //    //    return Task.FromResult(0);
            //    //});
            //    //await next();
            //});

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseCors("AllowOrigin");
                
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseStaticFiles();
            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Uploads")),
            //    RequestPath = new PathString("/Uploads")
            //});

            app.UseMvc();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute("default", "{controller=Values}/{action=Index}/{id?}");
            //});

        }


    }
}
